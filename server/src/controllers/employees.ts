import { Request, Response } from "express";
import { Employees, PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

interface EmployeesDTO {
  id: number;
  name: string;
  surname: string | null;
  ocupation: string | null;
  title: string | null;
  country: string | null;
  city: string | null;
}

export const getEmployees = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const employees: Employees[] = await prisma.employees.findMany();
  const employeesDTO: EmployeesDTO[] = employees.map((employee) => {
    return {
      id: employee.EmployeeID,
      name: employee.FirstName,
      surname: employee.LastName,
      ocupation: employee.Title,
      title: employee.TitleOfCourtesy,
      country: employee.Country,
      city: employee.City,
    };
  });
  return res.status(200).json(employeesDTO);
};

export const getEmployee = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { id } = req.params;

  if (!id || Number(id))
    return res
      .status(400)
      .json({ success: false, message: "id is required and must be number" });

  const employee: Employees | null = await prisma.employees.findUnique({
    where: { EmployeeID: Number(id) },
  });

  if (!employee)
    return res
      .status(404)
      .json({ success: false, message: "employee not found" });

  const employeeDTO: EmployeesDTO = {
    id: employee.EmployeeID,
    name: employee.FirstName,
    surname: employee.LastName,
    ocupation: employee.Title,
    title: employee.TitleOfCourtesy,
    country: employee.Country,
    city: employee.City,
  };

  return res.status(200).json({ employeeDTO });
};

export const createEmployee = async (
  req: Request,
  res: Response
): Promise<Response> => {
  console.log(req.body);
  const { name, surname, ocupation, title, country, city } = req.body;
  const validate: boolean = name && surname;

  if (!validate)
    return res
      .status(400)
      .json({ success: false, message: "name and surname are required" });

  const employee = await prisma.employees.create({
    data: {
      FirstName: name,
      LastName: surname,
      Title: ocupation,
      TitleOfCourtesy: title,
      Country: country,
      City: city,
    },
  });

  if (!employee)
    return res
      .status(500)
      .json({ success: false, message: "something went wrong" });

  return res
    .status(200)
    .json({ success: true, message: "employee succesfully created" });
};

export const deleteEmployee = async (req: Request, res: Response) => {
  const { id } = req.params;

  if (!id || !Number(id))
    return res
      .status(400)
      .json({ success: false, message: "id is required an d must be number" });

  const employee: Employees = await prisma.employees.delete({
    where: { EmployeeID: Number(id) },
  });

  if (!employee)
    return res
      .status(404)
      .json({ sucess: false, message: "employee not found" });

  return res
    .status(200)
    .json({ success: true, message: "employee succesfully deleted" });
};
