import { Router } from "express";
import { getCustomers, getCustomer } from "../controllers/customers";

const router: Router = Router();

router.get("/", getCustomers);
router.get("/:id", getCustomer);
// router.post("/", createEmployee);

export default router;
