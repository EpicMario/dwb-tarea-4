import express, { json, urlencoded } from "express";
import cors from "cors";
import employees from "./src/routes/employees";
import customers from "./src/routes/customers";
// import products from "./src/routes/products"; not finished

const main = async () => {
  const app = express();

  app.use(json());
  app.use(urlencoded({ extended: false }));
  app.use(cors());

  app.use("/api/employees", employees);
  app.use("/api/customers", customers);
  // app.use("/api/products", products); not finished

  app.get("/", (req, res) => {
    return res.status(200).send({ message: "hello world!" });
  });

  app.listen(8081, () => {
    console.log("Server running http://localhost:8081");
  });
};

main();
